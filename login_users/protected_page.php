<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
sec_session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Global Consulting.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">
    <!--
    @author Jose Peinado - Biinyu Games 
    -->
<link href="../main.css" rel="stylesheet"></head>
<body>
<?php if (login_check($mysqli) == true) : ?>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>    <div class="app-header__content">
                <!-- <div class="app-header-left">
                    <div class="search-wrapper">
                        <div class="input-holder">
                            <input type="text" class="search-input" placeholder="Type to search">
                            <button class="search-icon"><span></span></button>
                        </div>
                        <button class="close"></button>
                    </div>
                    <ul class="header-menu nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                <i class="nav-link-icon fa fa-database"> </i>
                                Statistics
                            </a>
                        </li>
                        <li class="btn-group nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                <i class="nav-link-icon fa fa-edit"></i>
                                Projects
                            </a>
                        </li>
                        <li class="dropdown nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                <i class="nav-link-icon fa fa-cog"></i>
                                Settings
                            </a>
                        </li>
                    </ul>        
                </div> -->
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" class="rounded-circle" src="../assets/images/avatars/1.jpg" alt="">
                                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <button type="button" tabindex="0" class="dropdown-item">Configurar Cuenta</button>
                                            <button type="button" tabindex="0" class="dropdown-item" onclick="location.href='./includes/logout.php';">Cerrar Sesión</button>
                                            <!-- <h6 tabindex="-1" class="dropdown-header">Header</h6>
                                            <button type="button" tabindex="0" class="dropdown-item">Actions</button>
                                            <div tabindex="-1" class="dropdown-divider"></div>
                                            <button type="button" tabindex="0" class="dropdown-item">Dividers</button> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content-left  ml-3 header-user-info">
                                    <div class="widget-heading">
                                    <?php echo htmlentities($_SESSION['username']); ?>
                                    </div>
                                    <div class="widget-subheading">
                                    <?php echo htmlentities($_SESSION['cargo']); ?>
                                    </div>
                                </div>
                                <div class="widget-content-right header-user-info ml-3">
                                    <button type="button" class="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example">
                                        <i class="fa text-white  fa-comments pr-1 pl-1"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </div>               
        <div class="app-main">
                <div class="app-sidebar sidebar-shadow">
                    <div class="app-header__logo">
                        <div class="logo-src"></div>
                        <div class="header__pane ml-auto">
                            <div>
                                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                    <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="app-header__mobile-menu">
                        <div>
                            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="app-header__menu">
                        <span>
                            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                <span class="btn-icon-wrapper">
                                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                                </span>
                            </button>
                        </span>
                    </div>    <div class="scrollbar-sidebar">
                        <div class="app-sidebar__inner">
                            <ul class="vertical-nav-menu">
                                <li class="app-sidebar__heading">Menú</li>
                                <li>
                                    <a href="index.html" class="mm-active">
                                        <i class="metismenu-icon pe-7s-rocket"></i>
                                        Resumen General
                                    </a>
                                </li>
                                <li class="app-sidebar__heading">Afiliados</li>
                                <li>
                                    <a href="gestion-afiliados.html">
                                        <i class="metismenu-icon pe-7s-diamond"></i>
                                        Gestión de afiliados
                                    </a>
                                </li>
                            </ul> 
                        </div>
                    </div>
                </div>    
                <div class="app-main__outer">
                    <div class="app-main__inner">                               
                        <div class="row">
                            <div class="col-md-6 col-xl-4">
                                <div class="card mb-3 widget-content bg-midnight-bloom">
                                    <div class="widget-content-wrapper text-white">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Total Prestamos</div>
                                            <div class="widget-subheading">Prestamos realizados</div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-white"><span>3</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="card mb-3 widget-content">
                                    <div class="widget-content-outer">
                                        <div class="widget-content-wrapper">
                                            <div class="widget-content-left">
                                                <div class="widget-heading">Cupo Total</div>
                                                <div class="widget-subheading">Valor en pesos m/c</div>
                                            </div>
                                            <div class="widget-content-right">
                                                <div class="widget-numbers text-success">$ 500.000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="col-md-6 col-xl-4">
                                <div class="card mb-3 widget-content bg-grow-early">
                                    <div class="widget-content-wrapper text-white">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Cuotas</div>
                                            <div class="widget-subheading">Numero de cuotas permitidas</div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-white"><span>4</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                    </div>    
                    <!-- Tabla de prestamos realizados-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-header">Historial de créditos
                                        <div class="btn-actions-pane-right">
                                            
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                            <thead>
                                            <tr>
                                             
                                                <th class="text-center">Numero de crédito</th>
                                                <th class="text-center">Fecha de desembolso</th>
                                                <th class="text-center">Numero de cuotas</th>
                                                <th class="text-center">Estado prestamo</th>
                                                <th class="text-center">Valor de prestamo</th>
                                                
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <!-- Créditos-->
                                            <tr>
                                                <!-- Numero de credito-->
                                                <td class="text-center text-muted"># 0001</td>
                                                <!-- Fecha de credito-->
                                                <td class="text-center">09/01/2020</td>
                                                <!-- Cuotas-->
                                                <td class="text-center">3</td>
                                                <!-- Estado de pago-->
                                                <td class="text-center">
                                                    <div class="badge badge-warning">Debe</div>
                                                </td>
                                                <!-- Valor-->
                                                <td class="text-center">$300.000</td>
                                                <!-- Detalle prestamo-->
                                               
                                            </tr>
                                            <!-- Fin Créditos-->
                                            <tr>
                                                <!-- Numero de credito-->
                                                <td class="text-center text-muted"># 0002</td>
                                                <!-- Fecha de credito-->
                                                <td class="text-center">09/01/2020</td>
                                                <!-- Cuotas-->
                                                <td class="text-center">3</td>
                                                <!-- Estado de pago-->
                                                <td class="text-center">
                                                    <div class="badge badge-warning">Debe</div>
                                                </td>
                                                <!-- Valor-->
                                                <td class="text-center">$500.000</td>
                                                <!-- Detalle prestamo-->
                                               
                                            </tr>
                                            <tr>
                                                <!-- Numero de credito-->
                                                <td class="text-center text-muted"># 0003</td>
                                                <!-- Fecha de credito-->
                                                <td class="text-center">09/01/2020</td>
                                                <!-- Cuotas-->
                                                <td class="text-center">3</td>
                                                <!-- Estado de pago-->
                                                <td class="text-center">
                                                    <div class="badge badge-warning">Debe</div>
                                                </td>
                                                <!-- Valor-->
                                                <td class="text-center">$200.000</td>
                                                <!-- Detalle prestamo-->
                                               
                                            </tr>                                   
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="d-block text-center card-footer">
                                        <button class="btn-wide btn btn-success">Solicitar nuevo crédito +</button>
                                        <button class="btn-wide btn btn-focus">Contactar al administrador</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                      
            
                                <script>
                                    // Example starter JavaScript for disabling form submissions if there are invalid fields
                                    (function() {
                                        'use strict';
                                        window.addEventListener('load', function() {
                                            // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                            var forms = document.getElementsByClassName('needs-validation');
                                            // Loop over them and prevent submission
                                            var validation = Array.prototype.filter.call(forms, function(form) {
                                                form.addEventListener('submit', function(event) {
                                                    if (form.checkValidity() === false) {
                                                        event.preventDefault();
                                                        event.stopPropagation();
                                                    }
                                                    form.classList.add('was-validated');
                                                }, false);
                                            });
                                        }, false);
                                    })();
                                </script>
                            </div>
                        </div>

                        <!--
                            Fin Formulario
                        -->
                        
                    </div>
                    <div class="app-wrapper-footer">
                        <div class="app-footer">
                            <div class="app-footer__inner">
                                <div class="app-footer-left">
                                    <ul class="nav">
                                        <li class="nav-item">
                                            <a href="javascript:void(0);" class="nav-link">
                                                &copy; Copyright Global Consulting SAS     - 
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link"href="http://www.biinyu.com.co">Sitio web desarrollado por Biinyu Games Studio</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>    </div>
                <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
<script type="text/javascript" src="../assets/scripts/main.js"></script>
<?php else : ?>
            <p>
                <span class="error">Lo siento, no estas autorizado para ingresar a esta página.</span> Por favor <a href="index.php">Inicia Sesión</a>.
            </p>
<?php endif; ?>
</body>
</html>

