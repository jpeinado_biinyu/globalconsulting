<!DOCTYPE html>
<!--
Copyright (C) 2013 peredur.net

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login: Registro Exitoso</title>
        <link rel="stylesheet" href="styles/main.css" />
    </head>
    <body>
        <h1>Registro de usuario exitoso!</h1>
        <p>Para iniciar sesión ingrese<a href="index.php">AQUÍ</a> e inicia</p>
    </body>
</html>
