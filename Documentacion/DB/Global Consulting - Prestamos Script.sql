create database global_consulting_prestamos;
use database global_consulting_prestamos;

-- -----------------------------------------------------
-- Table TIPO_IDENTIFICACION
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS TIPO_IDENTIFICACION (
  id_tipo_identificacion INT NOT NULL,
  sigla VARCHAR(4) NULL,
  nombre VARCHAR(15) NULL,
  estado VARCHAR(1) NULL,
  PRIMARY KEY (id_tipo_identificacion));

-- -----------------------------------------------------
-- Table USUARIOS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS USUARIOS (
  id_usuario INT NOT NULL AUTO_INCREMENT,
  id_tipo_identificacion INT NOT NULL,
  numero_identificacion VARCHAR(45) NOT NULL,
  primer_nombre VARCHAR(45) NOT NULL,
  segundo_nombre VARCHAR(45) NULL,
  primer_apellido VARCHAR(45) NOT NULL,
  fecha_registro DATE NOT NULL,
  PRIMARY KEY (id_usuario),
  INDEX fk_USUARIOS_TIPO_IDENTIFICACION_idx (id_tipo_identificacion ASC) VISIBLE,
  UNIQUE INDEX numero_identificacion_UNIQUE (numero_identificacion ASC) VISIBLE,
  CONSTRAINT fk_USUARIOS_TIPO_IDENTIFICACION
    FOREIGN KEY (id_tipo_identificacion)
    REFERENCES TIPO_IDENTIFICACION (id_tipo_identificacion)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table BANCOS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS BANCOS (
  id_banco INT NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  sucursal VARCHAR(45) NULL,
  estado VARCHAR(1) NULL,
  PRIMARY KEY (id_banco));


-- -----------------------------------------------------
-- Table TIPO_CUENTA
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS TIPO_CUENTA (
  id_tipo_cuenta INT NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  estado VARCHAR(1) NULL,
  PRIMARY KEY (id_tipo_cuenta));

-- -----------------------------------------------------
-- Table INFORMACION_BANCARIA
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS INFORMACION_BANCARIA (
  id_informacion_bancaria INT NOT NULL AUTO_INCREMENT,
  id_usuario INT NOT NULL,
  id_banco INT NOT NULL,
  id_tipo_cuenta INT NOT NULL,
  numero_cuenta VARCHAR(20) NOT NULL,
  estado VARCHAR(1) NOT NULL,
  PRIMARY KEY (id_informacion_bancaria),
  INDEX fk_INFORMACION_BANCARIA_BANCOS1_idx (id_banco ASC) VISIBLE,
  INDEX fk_INFORMACION_BANCARIA_TIPO_CUENTA1_idx (id_tipo_cuenta ASC) VISIBLE,
  INDEX fk_INFORMACION_BANCARIA_USUARIOS1_idx (id_usuario ASC) VISIBLE,
  CONSTRAINT fk_INFORMACION_BANCARIA_BANCOS1
    FOREIGN KEY (id_banco)
    REFERENCES BANCOS (id_banco)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_INFORMACION_BANCARIA_TIPO_CUENTA1
    FOREIGN KEY (id_tipo_cuenta)
    REFERENCES TIPO_CUENTA (id_tipo_cuenta)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_INFORMACION_BANCARIA_USUARIOS1
    FOREIGN KEY (id_usuario)
    REFERENCES USUARIOS (id_usuario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table TIPO_DATO_CONTACTO
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS TIPO_DATO_CONTACTO (
  id_tipo_dato_contacto INT NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  estado VARCHAR(1) NULL,
  PRIMARY KEY (id_tipo_dato_contacto));

-- -----------------------------------------------------
-- Table DATOS CONTACTO
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS DATOS CONTACTO (
  id_dato_contacto INT NOT NULL,
  id_tipo_dato_contacto INT NOT NULL,
  id_usuario INT NOT NULL,
  dato VARCHAR(60) NULL,
  estado VARCHAR(45) NULL,
  PRIMARY KEY (id_dato_contacto),
  INDEX fk_DATOS CONTACTO_TIPO_DATO_CONTACTO1_idx (id_tipo_dato_contacto ASC) VISIBLE,
  INDEX fk_DATOS CONTACTO_USUARIOS1_idx (id_usuario ASC) VISIBLE,
  CONSTRAINT fk_DATOS CONTACTO_TIPO_DATO_CONTACTO1
    FOREIGN KEY (id_tipo_dato_contacto)
    REFERENCES TIPO_DATO_CONTACTO (id_tipo_dato_contacto)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_DATOS CONTACTO_USUARIOS1
    FOREIGN KEY (id_usuario)
    REFERENCES USUARIOS (id_usuario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table USUARIO_LOGIN
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS USUARIO_LOGIN (
  id_usuario_login INT NOT NULL AUTO_INCREMENT,
  id_usuario INT NOT NULL,
  password VARCHAR(45) NOT NULL,
  salt VARCHAR(45) NOT NULL,
  PRIMARY KEY (id_usuario_login),
  INDEX fk_USUARIO_LOGIN_USUARIOS1_idx (id_usuario ASC) VISIBLE,
  CONSTRAINT fk_USUARIO_LOGIN_USUARIOS1
    FOREIGN KEY (id_usuario)
    REFERENCES USUARIOS (id_usuario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table TIPO_ESTADO_PRESTAMO
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS TIPO_ESTADO_PRESTAMO (
  id_tipo_estado_prestamo INT NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  estado VARCHAR(1) NOT NULL,
  PRIMARY KEY (id_tipo_estado_prestamo));

-- -----------------------------------------------------
-- Table PRESTAMOS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS PRESTAMOS (
  id_prestamo INT NOT NULL AUTO_INCREMENT,
  id_usuario INT NOT NULL,
  id_tipo_estado_prestamo INT NOT NULL,
  valor_solicitado DOUBLE NOT NULL,
  numero_cuotas INT NOT NULL,
  fecha_solicitud DATE NOT NULL,
  fecha_desembolso DATE NULL,
  fecha_pago DATE NOT NULL,
  num_cuotas_pagadas INT NOT NULL DEFAULT 0,
  num_cuotas_restantes INT NOT NULL,
  valor_cuota DOUBLE NOT NULL,
  PRIMARY KEY (id_prestamo),
  INDEX fk_PRESTAMOS_USUARIOS1_idx (id_usuario ASC) VISIBLE,
  INDEX fk_PRESTAMOS_TIPO_ESTADO_PRESTAMO1_idx (id_tipo_estado_prestamo ASC) VISIBLE,
  CONSTRAINT fk_PRESTAMOS_USUARIOS1
    FOREIGN KEY (id_usuario)
    REFERENCES USUARIOS (id_usuario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_PRESTAMOS_TIPO_ESTADO_PRESTAMO1
    FOREIGN KEY (id_tipo_estado_prestamo)
    REFERENCES TIPO_ESTADO_PRESTAMO (id_tipo_estado_prestamo)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table CUPO_USUARIO
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS CUPO_USUARIO (
  id_cupo_usuario INT NOT NULL AUTO_INCREMENT,
  id_usuario INT NOT NULL,
  fecha_inicio DATE NOT NULL,
  fecha_fin DATE NULL,
  valor_cupo DOUBLE NOT NULL,
  estado VARCHAR(1) NOT NULL,
  PRIMARY KEY (id_cupo_usuario),
  INDEX fk_CUPO_USUARIO_USUARIOS1_idx (id_usuario ASC) VISIBLE,
  CONSTRAINT fk_CUPO_USUARIO_USUARIOS1
    FOREIGN KEY (id_usuario)
    REFERENCES USUARIOS (id_usuario)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table PARAMETROS_NEGOCIO
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS PARAMETROS_NEGOCIO (
  id_parametro_negocio INT NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  descripcion VARCHAR(100) NOT NULL,
  valor VARCHAR(60) NOT NULL,
  PRIMARY KEY (id_parametro_negocio));

-- -----------------------------------------------------
-- Table PAGOS
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS PAGOS (
  id_pago INT NOT NULL,
  id_prestamo INT NOT NULL,
  valor_cuota DOUBLE NOT NULL,
  intereses_mora DOUBLE NOT NULL DEFAULT 0,
  fecha_pago DATE NOT NULL,
  PRIMARY KEY (id_pago),
  INDEX fk_PAGOS_PRESTAMOS1_idx (id_prestamo ASC) VISIBLE,
  CONSTRAINT fk_PAGOS_PRESTAMOS1
    FOREIGN KEY (id_prestamo)
    REFERENCES PRESTAMOS (id_prestamo)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

